<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Console\Input\InputArgument;

class PromoCodeCommand extends Command
{
    protected static $defaultName = 'promo-code:validate';

    private $offerUrl = 'https://601025826c21e10017050013.mockapi.io/ekwatest/offerList';
    private $promoUrl = 'https://601025826c21e10017050013.mockapi.io/ekwatest/promoCodeList';

    /**
     * configuration of the command
     */
    protected function configure(): void
    {
        $this
            ->addArgument('code', InputArgument::REQUIRED, 'code promo to search');
    }

    /**
     * command execution function
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $offers     = $this->getResponseApi($this->offerUrl);
        $promos     = $this->getResponseApi($this->promoUrl);
        $code       = $input->getArgument('code');

        $promotion  = $this->checkAndGetPromotionIfExist($promos, $code);
        if(empty($promotion)) {
            $output->writeln('No promotion found for the code: '.$input->getArgument('code'));
            return Command::FAILURE;
        }

        $output->writeln('Promotion found for the code: '.$input->getArgument('code'));

        $offerWithCode  = $this->checkAndGetOfferWithCode($offers, $code);
        if(empty($offerWithCode)) {
            $output->writeln('No offer found for the code: '.$input->getArgument('code'));
            return Command::FAILURE;
        }

        $output->writeln(count($offerWithCode) . ' offer(s) found for the code: ');

        $result = [
            'promoCode'             => $code,
            'endDate'               => $promotion['endDate'],
            'discountValue'         => $promotion['discountValue'],
            'compatibleOfferList'   => $offerWithCode
        ];

        $filename = 'CodePromo-' . $code . '-' . date('Y-m-d') . '.json';
        $location = './' . $filename;
        $fs = new \Symfony\Component\Filesystem\Filesystem();

        try {
            $fs->dumpFile($location, json_encode($result));
            $output->writeln( 'A file have been created containing promos. Filename : ' . $filename);
        }
        catch(IOException $e) {
            $output->writeln( 'A problem occurs during creation file');
            return Command::FAILURE;
        }
        $output->writeln(json_encode($result));

        return Command::SUCCESS;
    }

    /**
     * check if the offer exist and is valid
     *
     * @param array $offers
     * @param string $code
     * @return array
     */
    private function checkAndGetOfferWithCode(array $offers, string $code)
    {
        $offertab   = [];
        $i          = 0;
        foreach($offers as $offer)
        {
            if (in_array($code, $offer['validPromoCodeList'])) {
                $offertab[$i]['name'] = $offer['offerName'];
                $offertab[$i]['type'] = $offer['offerType'];
                $i++;
            }
        }

        return $offertab;
    }

    /**
     * check if the promotion exist and is valid
     *
     * @param array $promos
     * @param string $code
     * @return array|mixed
     */
    private function checkAndGetPromotionIfExist(array $promos, string $code)
    {
        $dateToday = strtotime(date('Y-m-d'));
        foreach($promos as $promo)
        {
            if ($promo['code'] === $code) {
                $datePromo = strtotime($promo['endDate']);
                if($dateToday < $datePromo) {
                    return $promo;
                }
            }
        }

        return [];
    }

    /**
     * get Api response
     *
     * @param string $url
     * @return array
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    private function getResponseApi(string $url) : array
    {
        $httpClient = HttpClient::create();

        $response = $httpClient->request('GET', $url);
        $statusCode = $response->getStatusCode();
        if ($statusCode === 200) {
            $content = $response->getContent();
            return json_decode($content, true);
        }

        return [];
    }
}