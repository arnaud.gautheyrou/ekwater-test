<?php

namespace App\Tests\Command;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class PromoCodeCommandTest extends KernelTestCase
{
    public function testExecuteWork()
    {
        $kernel         = static::createKernel();
        $application    = new Application($kernel);

        $command = $application->find('promo-code:validate');

        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'code' => 'ELEC_N_WOOD',
        ]);

        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('A file have been created containing promos', $output);
    }

    public function testExecuteWithFalsePromotionCode()
    {
        $kernel         = static::createKernel();
        $application    = new Application($kernel);

        $command = $application->find('promo-code:validate');

        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'code' => 'I_DONT_WORK',
        ]);

        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('No promotion found', $output);
    }

    public function testExecuteWithPromotionCodeExpirationDate()
    {
        $kernel         = static::createKernel();
        $application    = new Application($kernel);

        $command = $application->find('promo-code:validate');

        $commandTester = new CommandTester($command);
        $commandTester->execute([
            'code' => 'EKWA_WELCOME',
        ]);

        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('No promotion found for the code: EKWA_WELCOME', $output);
    }
}